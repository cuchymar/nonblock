package cz.esw.nonblock;

import java.util.Objects;
import java.util.function.IntFunction;

/**
 * @author Marek Cuchý
 */
public class QueueTest {

	public static void main(String[] args) {
		testQueue(SynchronizedArrayQueue::new);
		testQueue(NonblockingArrayQueue::new);
	}

	private static void testQueue(IntFunction<StringQueue> queueFactory) {
		StringQueue queue = queueFactory.apply(3);
		queue.offer("a");
		queue.offer("b");
		assertEquals("a", queue.poll());
		assertEquals("b", queue.poll());
		queue.offer("c");
		queue.offer("d");
		queue.offer("e");
		queue.offer("f");
		queue.offer("g");
		assertEquals("c", queue.poll());
		assertEquals("d", queue.poll());
		assertEquals("e", queue.poll());

		assertNull(queue.poll());
		assertNull(queue.poll());
		assertNull(queue.poll());
		assertNull(queue.poll());
		queue.offer("h");
		queue.offer("i");
		queue.offer("j");
		queue.offer("k");

		assertEquals("h", queue.poll());
		assertEquals("i", queue.poll());
		assertEquals("j", queue.poll());

		assertNull(queue.poll());
		assertNull(queue.poll());
		assertNull(queue.poll());
		assertNull(queue.poll());

	}

	//not necessary to add junit to classpath
	private static void assertNull(String object) {
		if (object != null) {
			throw new IllegalStateException();
		}
	}

	private static void assertEquals(Object expected, Object actual) {
		if (!Objects.equals(expected, actual)) {
			throw new IllegalStateException();
		}
	}

	private static void assertEquals(int expected, int actual) {
		if (expected != actual) {
			throw new IllegalStateException();
		}
	}

}