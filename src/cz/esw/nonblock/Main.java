package cz.esw.nonblock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author Marek Cuchý
 */
public class Main {

	private final static int MAX_THREADS = 7;
	private final static float OFFER_PORTION = 0.5f;

	private final static int TOTAL_BENCHMARK_OPERATIONS = 100_000_000;
	private final static int REPEAT_BENCHMARK = 10;

	private final static int ARRAY_SIZE = 100_000;

	private static long test(int threads, StringQueue queue) throws InterruptedException, ExecutionException {

		ExecutorService executor = Executors.newFixedThreadPool(threads);

		List<Stat> stats = new ArrayList<>();
		List<QueueWriter> writers = new ArrayList<>();
		for (int i = 0; i < threads; i++) {
			writers.add(new QueueWriter(TOTAL_BENCHMARK_OPERATIONS / threads, queue, OFFER_PORTION));
		}

		long t1 = System.nanoTime();
		List<Future<Stat>> futures = executor.invokeAll(writers);
		executor.shutdown();
		executor.awaitTermination(1, TimeUnit.HOURS);
		long t2 = System.nanoTime();

		for (Future<Stat> future : futures) {
			stats.add(future.get());
		}

		//		System.out.println("Thread stats: ");
		//		Stat total = new Stat();
		//		stats.forEach(total::add);
		//		stats.forEach(System.out::println);
		//		System.out.println("TOTAL: " + total);

		System.out.println(
				threads + " thread(s) " + queue.getClass().getSimpleName() + " TIME: " + (t2 - t1) / 1_000_000 + " " +
				"ms");

		return t2 - t1;
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		for (int i = 1; i < MAX_THREADS; i++) {
			int fasterNonblocking = 0;
			for (int j = 0; j < REPEAT_BENCHMARK; j++) {
				if (test(i, new NonblockingArrayQueue(ARRAY_SIZE)) < test(i, new SynchronizedArrayQueue(ARRAY_SIZE))) {
					fasterNonblocking++;
				}
			}
			System.out.println("Non-blocking was in " + fasterNonblocking + " cases faster than Synchronized (from " +
							   REPEAT_BENCHMARK + " runs) with " + i + " thread(s)");
		}
	}
}
