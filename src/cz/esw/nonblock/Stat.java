package cz.esw.nonblock;

/**
 * @author Marek Cuchý
 */
public class Stat {

	private int offered;
	private int full;
	private int polled;
	private int empty;

	public void offered() {
		offered++;
	}

	public void full() {
		full++;
	}

	public void polled() {
		polled++;
	}

	public void empty() {
		empty++;
	}

	public void add(Stat stat) {
		offered += stat.offered;
		full += stat.full;
		polled += stat.polled;
		empty += stat.empty;
	}

	@Override
	public String toString() {
		return "Stat [" + "offered=" + offered + ", full=" + full + ", polled=" + polled + ", empty=" + empty +
			   ']';
	}
}
